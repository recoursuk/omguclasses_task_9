package lambdaDemo;

import java.util.Objects;

public class Human {
    private String name;
    private String patronymic;
    private String lastName;
    private int age;
    private Sex sex;

    public Human(String name, String patronymic, String lastName, int age, Sex sex) {
        this.name = name;
        this.patronymic = patronymic;
        this.lastName = lastName;
        this.age = age;
        this.sex = sex;
    }

    public Human(Human other){
        this.sex = other.sex;
        this.age = other.age;
        this.name = other.name;
        this.patronymic = other.patronymic;
        this.lastName = other.lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return Objects.equals(name, human.name) &&
                Objects.equals(patronymic, human.patronymic) &&
                Objects.equals(lastName, human.lastName) &&
                Objects.equals(age, human.age) &&
                sex == human.sex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, patronymic, lastName, age, sex);
    }

    @Override
    public String toString() {
        return '\n'+"Human{" +
                "name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                '}';
    }

    public static int compareBySex(Human that, Human other){
        if(that.sex.equals(Sex.FEMALE) && other.sex.equals(Sex.MALE)){
            return 1;
        }
        else if (that.sex.equals(Sex.MALE) && other.sex.equals(Sex.FEMALE)){
            return -1;
        }
        else return 0;
    }
}
