package lambdaDemo;

import java.util.function.BiFunction;
import java.util.function.Function;

public final class LambdaDemo {
    public static final Function<String, Integer> getLength =
            String::length;
    public static final Function<String, Character> getFirstSymbol =
            string -> ("".equals(string) ? null : string.charAt(0));
    public static final Function<String, Boolean> checkSpaceExistence =
            string -> string == null || !string.contains(" ");
    public static final Function<String, Integer> countWords =
            string -> ("".equals(string) ? 0 : string.split(",").length);
    public static final Function<Human, Integer> getHumanAge =
            Human::getAge;
    public static final BiFunction<Human,Human, Boolean> checkEqualityOfLastNames =
            (human1, human2) -> human1.getLastName().equals(human2.getLastName());
    public static final Function<Human, Human> getOlderHuman =
            human ->
            new Human(human.getName(), human.getPatronymic(), human.getLastName(), human.getAge()+1, human.getSex());
    public static final Function<Human, String> getFullName =
            human -> human.getName() + " " + human.getLastName() + " " + human.getPatronymic();
    public static final QuadFunction<Boolean> checkAllYounger = (human1, human2, human3, maxAge) ->
        human1.getAge()<maxAge && human2.getAge()<maxAge && human3.getAge()<maxAge;
}

