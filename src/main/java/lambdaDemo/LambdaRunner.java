package lambdaDemo;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

public class LambdaRunner {
    public static <T,R> R run(Function<T, R> lambda, T arg){
        return lambda.apply(arg);
    }
    public static void sort(SortHumans lambda, Collection<Human> humans){
        lambda.sort(humans);
    }

    public static <T, U, R> R run(BiFunction<T, U, R> lambda, T arg1, U arg2){
        return lambda.apply(arg1, arg2);
    }

    public static Boolean checkAllYounger(QuadFunction<Boolean> owth, Human firstHuman, Human secondHuman, Human thirdHuman, int maxAge){
        return owth.calculate(firstHuman, secondHuman, thirdHuman, maxAge);
    }

}
