package lambdaDemo;

public interface QuadFunction<T> {
    T calculate(Human firstHuman, Human secondHuman, Human thirdHuman, int maxAge);
}
