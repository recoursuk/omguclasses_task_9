package lambdaDemo;

import java.util.Collection;

public interface SortHumans{
    void sort (Collection<Human> humans);
}
