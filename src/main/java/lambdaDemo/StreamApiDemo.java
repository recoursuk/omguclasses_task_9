package lambdaDemo;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamApiDemo {
    public static Function<List<Object>, List<Object>> deleteAllNullObjects =
            l -> l.stream().filter(Objects::nonNull).collect(Collectors.toList());//1
    public static Function<Set<Integer>, Integer> findAmountOfPositiveNumbers =
            s -> Math.toIntExact(s.stream().filter(integer -> integer > 0).count());//2
    public static Function<List<Object>,List<Object>> getLastThree =
            list -> list.size() -3 <=0 ? list : list.stream().skip(list.size()-3).collect(Collectors.toList());
    public static Function<List<Integer>, Integer> findFirstEven =
            l -> l.stream().filter(integer -> integer%2 == 0).findFirst().orElse(null);//4
    public static Function<Integer[], List<Integer>> getSquaredList =
            integers -> Arrays.stream(integers).map(integer -> integer*integer).distinct().collect(Collectors.toList());//5
    public static Function<List<String>, List<String>> removeAllEmptyStringsAndSort =
            list -> list.stream().filter(string -> !"".equals(string)).sorted().collect(Collectors.toList());//6
    public static Function<Set<String>, List<String>> sortStringSetReversedOrder =
            set -> set.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());//7
    public static Function<Set<Integer>, Integer> getSumOfSquaredElements =
            set -> set.stream().map(integer -> integer*integer).reduce(Integer::sum).orElse(0);//8
    public static Function<Collection<Human>, Integer> getMaxAge =
            col -> col.stream().max((Comparator.comparingInt(Human::getAge))).get().getAge();//9
    public static Function<Collection<Human>, Collection<Human>> sortHumansBySex =
            col -> col.stream().sorted(Human::compareBySex).collect(Collectors.toList());//10
    public static Function<Collection<Human>, Collection<Human>> sortHumansByAge =
            col -> col.stream().
                    sorted(Comparator.
                            comparing(Human::getSex).
                            thenComparing(Human::getAge)
                    ).
                    collect(Collectors.toList());//11
}