package lambdaDemo;

import java.util.Objects;

public class Student extends Human {
    private String university;
    private String faculty;
    private String specialization;

    public Student(String name, String patronymic, String lastName, int age, Sex sex, String university, String faculty, String specialization) {
        super(name, patronymic, lastName, age, sex);
        this.university = university;
        this.faculty = faculty;
        this.specialization = specialization;
    }
    public Student(Human human, String university, String faculty, String specialty){
        super(human);
        this.university = university;
        this.faculty = faculty;
        this.specialization = specialty;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return Objects.equals(university, student.university) &&
                Objects.equals(faculty, student.faculty) &&
                Objects.equals(specialization, student.specialization);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), university, faculty, specialization);
    }
}
