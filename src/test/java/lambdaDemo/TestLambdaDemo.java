package lambdaDemo;

import org.junit.Assert;
import org.junit.Test;

import static lambdaDemo.LambdaRunner.run;


public class TestLambdaDemo {

    @Test
    public void testGetLength(){
        Assert.assertEquals(0, (int) run(LambdaDemo.getLength, ""));
        Assert.assertEquals(3, (int) run(LambdaDemo.getLength, "abc"));
        Assert.assertEquals(1, (int) run(LambdaDemo.getLength, "a"));
    }

    @Test
    public void testGetFirstSymbol(){
        Assert.assertEquals('a', (char) run(LambdaDemo.getFirstSymbol, "abc"));
        Assert.assertEquals('.', (char) run(LambdaDemo.getFirstSymbol, ".bc"));
    }

    @Test
    public void testDoesStringConsistSpaces(){
        Assert.assertFalse(run(LambdaDemo.checkSpaceExistence, " "));
        Assert.assertFalse(run(LambdaDemo.checkSpaceExistence, " a b c "));
    }

    @Test
    public void testAmountOfWordsInString(){
        Assert.assertEquals(0, (int) run(LambdaDemo.countWords, ""));
        Assert.assertEquals(1, (int) run(LambdaDemo.countWords, "one"));
        Assert.assertEquals(2, (int) run(LambdaDemo.countWords, "one, two"));
    }

    @Test
    public void testGetHumanAge(){
        Human human = new Human("Jane", "Huston", "Third",23, Sex.FEMALE);
        Student student = new Student(human, "Harvard", "Biology Faculty", "Programmer");
        Assert.assertEquals(23, (int) run(LambdaDemo.getHumanAge, student));
    }

    @Test
    public void testIsSecondNamesEqual(){
        Human human = new Human( "Jane", "Huston", "T", 23, Sex.FEMALE);
        Human secondHuman = new Human("Jane", "Jonson", "B", 23,Sex.FEMALE);
        Student student = new Student(human, "Harvard", "Biology Faculty", "Programmer");
        Assert.assertTrue(run(LambdaDemo.checkEqualityOfLastNames, human, student));
        Assert.assertFalse(run(LambdaDemo.checkEqualityOfLastNames, human, secondHuman));
    }

    @Test
    public void testGetFullName(){
        Human human = new Human("Jane", "Huston", "Third", 23, Sex.FEMALE);
        Assert.assertEquals("Jane Third Huston", run(LambdaDemo.getFullName, human));
    }

    @Test
    public void testMakeHumanOneYearOlder(){
        Human human = new Human("Jane", "Huston", "Third", 23, Sex.FEMALE);
        Human expected = new Human("Jane", "Huston", "Third", 24, Sex.FEMALE);
        Assert.assertEquals(expected, run(LambdaDemo.getOlderHuman, human));
    }

    @Test
    public void testCheckAllYounger(){
        Human firstHuman = new Human( "Jane", "Huston", "First", 23, Sex.FEMALE);
        Human secondHuman = new Human( "Jane", "Huston", "Second", 23, Sex.FEMALE);
        Human thirdHuman = new Human( "Jane", "Huston", "Third", 23, Sex.FEMALE);
        Assert.assertTrue(LambdaRunner.checkAllYounger(LambdaDemo.checkAllYounger, firstHuman, secondHuman, thirdHuman, 34));
    }
}
